
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE foo (
  id SERIAL,
  name VARCHAR,
  PRIMARY KEY (id)
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE foo;

